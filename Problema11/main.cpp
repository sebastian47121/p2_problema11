#include <iostream>

using namespace std;

void llenar_asientos(char asientos[15][20]){    //Funcion para recorrer la matriz uno a uno para ser llanado por '-'

    for (int filas=0;filas<15;filas++){         //Recorre cada fila
        for (int sits=0; sits < 20; sits++){        //Por cada fila recorrida, recorre 20 asientos
            asientos[filas][sits]='-';              //Se le asigna el caracter '-' a la casilla actual
        }
    }
}

void imprimir_asientos(char asientos[15][20]){      //Funcion para recorrer la maatriz uno a uno e imprimir su valor, o en este caso caracter

    char letras[26]={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
    cout << "  ";

    for (int nums=1;nums <=20;nums++){              //Ciclo para imprimir los numeros superiores que van desde el 1, hasta el 20
        cout <<nums<<" ";
    }
    cout << "\n";
    for (int filas=0;filas<15;filas++){             //Ciclo que recorre cada fila

        cout << letras[filas]<<" ";                //Se imprime la letra respectiva al numero de fila por medio del arreglo letras, y el numero de fila actual

        for (int sits=0; sits < 20; sits++){        //Ciclo que recorre cada silla de la fila actual
            if (sits >=9){  //Condicion solo por estetica, para que hayan mas espacio al haber ya numeros de dos cifras arriba de ellos
                cout <<" "<< asientos[filas][sits]<<" ";        //Imprime el valor en la fila y asiento actual, incluyendo los espacios extra
            }
            else
                cout << asientos[filas][sits]<<" ";         //Imprime el valor en la fila y asiento actual
        }
        cout << "\n";
    }

    cout << "\n";
}

int main()
{
    /* Este programa  contiene un arreglo tipo matriz que contiene los asientos disponibles y reservados, permitiendo al usuario reservar
    y cancelar asientos al elegir la fila y el asiento de dicha fila, podiendose realizar las veces que el usuario requiera*/

    char asientos[15][20]; //Se crea la matriz
    bool continuar = true;  //Bool para verificar si se requiere repetir el proceso
    char eleccion,letraContinuar,filaElegida;
    int codFila,numeroAsiento;

    llenar_asientos(asientos);  //funcion que llena la matriz con '-'

    cout << "Lista de asientos:"<<endl;
    cout <<"\n";
    imprimir_asientos(asientos);  //funcion que imprime la matriz al recorrerla

    while(continuar == true){

        cout <<"Desea realizar una reserva o cancelacion? (r/c) "<<endl;
        cin >> eleccion;
        if (eleccion == 'R' || eleccion == 'r'){
            cout <<"Ingrese la fila: (A-O)"<<endl;
            cin >> filaElegida;
            filaElegida = toupper(filaElegida);         //Pone la letra ingresada en mayuscula para mas facilidad
            codFila=int(filaElegida);                   //Convierte la letra en codigo ASCII
            if (codFila >= 65 && codFila <=79){             //Verifica que la letra este en el rango (A-O) mediante codigo ASCII
                codFila-=65;                            //Se le resta 65 para tener el codigo como valor dentro de la matriz
                cout <<"Ingrese el asiento: (1-20) "<<endl;
                cin >> numeroAsiento;
                if (numeroAsiento>= 1 && numeroAsiento <=20){       //Verifica que el numero de asiento este en el rango (1-20)
                    numeroAsiento-=1;                               //Se le resta 1 para tener el numero como valor dentro de la matriz
                    if (asientos[codFila][numeroAsiento]== '-'){    //Verifica que el asiento este disponible
                        asientos[codFila][numeroAsiento]='+';        //Se cambia ese asiento a reservado
                        cout <<"Reserva exitosa."<<endl;
                        imprimir_asientos(asientos);                //Imprime de nuevo la matriz
                    }
                    else
                        cout <<"La silla ya se encuentra reservada."<<endl;

                }
                else
                    cout <<"La silla no existe."<<endl;
            }
            else
                cout << "La fila no existe."<<endl;
        }
        else if (eleccion == 'c' || eleccion == 'C'){
            cout <<"Ingrese la fila: (A-O)"<<endl;
            cin >> filaElegida;
            filaElegida = toupper(filaElegida);               //Pone la letra ingresada en mayuscula para mas facilidad
            codFila=int(filaElegida);                        //Convierte la letra en codigo ASCII
            if (codFila >= 65 && codFila <=79){             //Verifica que la letra este en el rango (A-O) mediante codigo ASCII
                codFila-=65;                                //Se le resta 65 para tener el codigo como valor dentro de la matriz
                cout <<"Ingrese el asiento: (1-20) "<<endl;
                cin >> numeroAsiento;
                if (numeroAsiento>= 1 && numeroAsiento <=20){           //Verifica que el numero de asiento este en el rango (1-20)
                    numeroAsiento-=1;                                   //Se le resta 1 para tener el numero como valor dentro de la matriz
                    if (asientos[codFila][numeroAsiento]=='+'){         //Verifica que el asiento este reservado
                        asientos[codFila][numeroAsiento]='-';           //Se cambia ese asiento a disponible
                        cout << "Cancelacion exitosa."<<endl;
                        imprimir_asientos(asientos);                    //Imprime de nuevo la matriz
                    }
                    else
                        cout <<"El asiento ya se encuentra disponible."<<endl;

                }
                else
                    cout <<"La silla no existe."<<endl;
            }
            else
                cout << "La fila no existe."<<endl;

        }
        else
            cout << "Ha ingresado una letra incorrecta."<<endl;

        cout << "Desea repetir el proceso? (y) "<<endl;
        cin >> letraContinuar;
        if (letraContinuar == 'y' || letraContinuar == 'Y')         //Se le pregunta al usuario si desea repetir el proceso
            continuar = true;                                       //al ingresar Y o y, el bool continuar pasa a true, y el proceso se repite
        else
            continuar=false;


    }
    cout <<"\n";
    cout << "Hasta Pronto!"<<endl;

    return 0;
}
